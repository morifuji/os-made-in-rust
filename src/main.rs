#![no_std]
#![no_main]
#![feature(custom_test_frameworks)]
#![test_runner(rust_os::test_runner)]
#![reexport_test_harness_main = "test_main"]
use core::panic::PanicInfo;
use rust_os::println;
use bootloader::{BootInfo, bootinfo::{MemoryMap, MemoryRegionType}, entry_point};
use x86_64::structures::paging::{FrameAllocator, PhysFrame, Size4KiB};
use x86_64::PhysAddr;

extern crate alloc;

use rust_os::allocator;

use alloc::{boxed::Box, vec, vec::Vec, rc::Rc};

entry_point!(kernel_main);

fn kernel_main(boot_info: &'static BootInfo) -> ! {

    // use rust_os::memory::active_level_4_table;
    // use x86_64::VirtAddr;
    // use x86_64::structures::paging::PageTable;

    use rust_os::memory;
    use x86_64::{structures::paging::Page, structures::paging::Translate, VirtAddr};

    let mut frame_allocator = unsafe {memory::BootInfoFrameAllocator::init(&boot_info.memory_map)};

    println!("Hello World{}", "!");
    rust_os::init();

    let phys_mem_offset = VirtAddr::new(boot_info.physical_memory_offset);
    // mapper
    let mut mapper = unsafe { memory::init(phys_mem_offset) };


    // // 
    // // // frame allocator, この段階ではNoneを必ず返す
    // // let mut frame_allocator = memory::EmptyFrameAllocator;

    // // let page = Page::containing_address(VirtAddr::new(0));
    // let page = Page::containing_address(VirtAddr::new(0xdeadbeaf000));
    
    // memory::create_example_mapping(page, &mut mapper, &mut frame_allocator);
    
    // let page_pointer: *mut u64 = page.start_address().as_mut_ptr();
    // unsafe {page_pointer.offset(400).write_volatile(0x_f021_f077_f065_f04e)};

    // let addresses = [
    //     // VGAなどの必ず物理アドレスと仮想アドレスが一致するアドレス
    //     0xb8000,
    //     // コード配置部分、readonlyなはず？
    //     0x201008,
    //     // スタック配置部分
    //     0x0100_0020_1a10,
    //     // 物理アドレス0に位置する仮装アドレス
    //     boot_info.physical_memory_offset
    // ];

    // for &address in &addresses {
    //     let virt = VirtAddr::new(address);
    //     let phys = mapper.translate_addr(virt);
    //     // let phys = unsafe { translate_addr(virt, phys_mem_offset) };
    //     println!("{:?} -> {:?}", virt, phys);
    // }

    // ページフォールと
    // let ptr = 0x2031b2 as *mut u32;
    // unsafe {
    //     let x = *ptr;
    // }
    // println!("read worked");
    // unsafe {
    //     *ptr = 42;
    // }
    // println!("write worked");

    // fn stack_overflow() {
    //     stack_overflow(); // 再帰呼び出しのために、リターンアドレスがプッシュされる
    // }

    // stack_overflow();

    // ページフォールと
    // unsafe {
    //     *(0xdeadbeef as *mut u64) = 42;
    // }

    // // ブレークポイントの招待
    // x86_64::instructions::interrupts::int3();

    #[cfg(test)]
    test_main();


    // new
    allocator::init_heap(&mut mapper, &mut frame_allocator).expect("heap initialization failed");

    let heap_value = Box::new(0u16);
    println!("0u16 at {:p}", heap_value);
    let heap_value = Box::new(0u32);
    println!("0u32 at {:p}", heap_value);
    let heap_value = Box::new(0u64);
    println!("0u64 at {:p}", heap_value);
    let heap_value = Box::new(0u128);
    println!("0u128 at {:p}", heap_value);

    let heap_value = Box::new(0i16);
    println!("0i16 at {:p}", heap_value);
    let heap_value = Box::new(0i32);
    println!("0i32 at {:p}", heap_value);
    let heap_value = Box::new(0i64);
    println!("0i64 at {:p}", heap_value);
    let heap_value = Box::new(0i128);
    println!("0i128 at {:p}", heap_value);    

    // create a dynamically sized vector
    let mut vec = Vec::new();
    for i in 0..500 {
        vec.push(i);
    }
    println!("vec at {:p}", vec.as_slice());

    // create a reference counted vector -> will be freed when count reaches 0
    let reference_counted = Rc::new(vec![1, 2, 3]);
    let cloned_reference = reference_counted.clone();
    println!("current reference count is {}", Rc::strong_count(&cloned_reference));
    core::mem::drop(reference_counted);
    println!("reference count is {} now", Rc::strong_count(&cloned_reference));

    
    println!("It did not crash!");

    // loop {}
    // hlt instruction
    rust_os::hlt_loop();
}

/// This function is called on panic.
#[cfg(not(test))]
#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    println!("{}", info);
    rust_os::hlt_loop();
}

#[cfg(test)]
#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    rust_os::test_panic_handler(info)
}

#[test_case]
fn trivial_assertion() {
    assert_eq!(1, 1);
}
