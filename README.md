## OS made in Rust

こちらのサイトを参考にRustでOSを作る

>https://os.phil-opp.com/ja/


## 発見したこと

### フリースタンディングな Rust バイナリ

#### never型（`!`）

帰り値がない場合はnever型を利用する。

never型にすることで不要な推論・処理にwarningを出せたりコンパイラがより最適化される

>https://stackoverflow.com/a/51835567/10040146

#### language item

コンパイラ内部で利用されている関数や型。

#### アンワインド

エラーが発生した時、呼び出し元の関数に戻っていくが、通常とは違い`try`句に到達するまで呼び出し元に戻っていくことをいうらしい。挙動はわかるが名前がついていたのは知らなかった

>https://www.bogotobogo.com/cplusplus/stackunwinding.php

#### `error: requires `start` lang_item`

ランタイムシステムの初期化が必要らしい

#### `no_mangle`

>Rust コンパイラが _start という名前の関数を実際に出力するように、#[no_mangle] attributeを用いて名前修飾を無効にします。この attribute がないと、コンパイラはすべての関数にユニークな名前をつけるために、 _ZN3blog_os4_start7hb173fedf945531caE のようなシンボルを生成します。

#### リンカ

ビルド=プリプロセス→コンパイル→アセンブル→リンカで、 各`obj`ファイルを結びつける処理

#### ABI

APIと対義的に使われているらしい。binaryのインタフェース。自分のMacだとHostが`x86_64-apple-darwin`なのでABIがない？

>https://en.wikipedia.org/wiki/Application_binary_interface

### Rustでつくる最小のカーネル

#### 起動プロセス

- ROMからファームウェアのコードを読み取り
- 自己テストを行い
- 使用可能なRAMを探しCPUとハードウェアを初期化する
- ブータブルディスクを探し出し
- ブートローダーを起動させる
  - ブートローダーは512バイト
    - 512バイトを超えることがほとんど
    - 
  - 
- OSのカーネルを起動

#### coreライブラリ

>失敗しましたね！エラーはRustコンパイラがcoreライブラリを見つけられなくなったと言っています。このライブラリは、Result や Option、イテレータのような基本的なRustの型を持っており、暗黙のうちにすべてのno_stdなクレートにリンクされています。


#### ブートローダーとリンクする

カーネルをブートローダとリンクさせて`ブータブルディスクイメージ`にする。

ブートローダー=CPUを初期化しカーネルをロードする役割

### VGAテキストモード

#### VGA

プロジェクターを設定する時とかによく見るやつ。どうやらVGA接続とDVI接続の2種類があるらしい。

>https://www.pc-master.jp/jisaku/vga-dvi.html



>VGAテキストバッファは、普通25行と80列からなる2次元配列で、画面に直接書き出されます。それぞれの配列の要素は画面上の一つの文字を以下の形式で表現しています：

#### `repr(u8)`

付与した構造体のフィールドのサイズを`u8`にする。

>フィールドの順序、サイズ、アラインメントが、C や C++ に期待するのと全く同じになります。FFI 境界を超えるであろう型は、すべて repr(C) になるべきです。 C はプログラミング界の共通言語なのですから。 また、値を別の型として再解釈する、といった複雑なトリックをやる場合にも repr(C) は必須です
>
>https://doc.rust-jp.rs/rust-nomicon-ja/other-reprs.html

#### `repr(transparent)`

1つのフィールドと同じサイズでメモリのレイアウトが組まれることを保証するための属性？

>https://doc.rust-lang.org/nomicon/other-reprs.html#reprtransparent


#### `repr(C)`

Rustでの構造体の並び方は未定義のため、体のフィールドがCの構造体と全く同じように並べられることを保証するために利用している

>https://doc.rust-jp.rs/rust-nomicon-ja/other-reprs.html#reprc

#### `Volatile`

読み込みされていない値に関連する処理をコンパイラが勝手に消さないよう指示することを`volatile`と指すらしい

`C++`の文脈での`volatile`の記事↓

>https://qiita.com/YukiMiyatake/items/1191ab03b6c0b5a22876



### CPU例外

#### x86の例外

例外：エラー時にCPUが割り込む割り込む処理のことをさす

[x86の例外](https://wiki.osdev.org/Exceptions)は20種類ほど。

大きく分けて3種類に分類できる

1. Faults: 復帰可能な例外
1. Traps: トラップの命令直後に報告される
1. Aborts: 復帰不可能な例外

#### 割り込み記述子表

通称IDTと呼ばれる

>https://wiki.osdev.org/Exceptions

#### 呼出規約

x86_64上のLinuxでは、C言語の関数に関しては、

- 最初の6つの整数引数は、レジスタrdi, rsi, rdx, rcx, r8, r9で渡される
- 追加の引数はスタックで渡される
- 結果はraxとrdxで返される

というルールが適用される。Rustでは`extern "C" fn`と書かれている場合この規約に沿う

>https://en.wikipedia.org/wiki/Calling_convention

#### デバッグ例外

ブレークポイントを置いたとき、内部的には命令を書き換え、例外を発生させている。

処理を継続する時は命令をもとに書き換えて次の処理に進む

>https://eli.thegreenplace.net/2011/01/27/how-debuggers-work-part-2-breakpoints

#### 
###

#### 発散する

前述のnever型（=`!`）のこと。

>https://doc.rust-jp.rs/rust-by-example-ja/fn/diverging.html

#### ダブルフォルトの原因

任意の例外発生中にさらに例外が発生したときにダブルフォルトになるのではない。**特定の**例外発生中に**特定の例外**が発生すると起こる

ex) ゼロ除算→セグメント不在, 無効TSS→一般保護違反

したがって、ブレークポイント例外が発生して対応するハンドラ関数がスワップアウトされている場合、**ページフォルトが発生して(ダブルフォルトハンドラではなく)ページフォルトハンドラが呼び出される**

#### カーネルがスタックをオーバーフローさせてガードページにヒットしたら？

ガードページはメモリ上に実在しない、したがってページフォールトが発生し、さらにスタックに`割り込みスタックフレーム`を積もうとする、しかしまだガードページを指しているためダブルフォールト, トリプルフォルトが発生してしまう。

この問題を避けるには、**スタックを切り替える**という方法がある

>x86_64アーキテクチャは例外発生時に予め定義されている既知の正常なスタックに切り替えることができます。この切り替えはハードウェアレベルで発生するので、CPUが例外スタックフレームをプッシュする前に行うことができます。
>
>https://os.phil-opp.com/ja/double-fault-exceptions/#sutatukuwoqie-riti-eru

#### 割り込みスタックテーブル

例外発生時に予め定義されたスタックに切り替えることができる。例外の種類によってどのスタックに切り替えるかは**割り込みスタックテーブル（=IST）**で定義されている。ISTは7つ。

ISTは**タスクステートセグメント（=TSS）**というレガシーな構造体の一部


TSSの構成↓

| フィールド               | 型       |
|--------------------------|----------|
| (予約済み)               | u32      |
| 特権スタックテーブル     | [u64; 3] |
| (予約済み)               | u64      |
| 割り込みスタックテーブル | [u64; 7] |
| (予約済み)               | u64      |
| (予約済み)               | u16      |
| I/Oマップベースアドレス  | u16      |

>https://os.phil-opp.com/ja/double-fault-exceptions/#isttotss


`x86_64`モジュールの定義をみても同じ構成だった

```rust
// x86_64-0.13.6/src/structures/tss.rs

/// In 64-bit mode the TSS holds information that is not
/// directly related to the task-switch mechanism,
/// but is used for finding kernel level stack
/// if interrupts arrive while in kernel mode.
#[derive(Debug, Clone, Copy)]
#[repr(C, packed)]
pub struct TaskStateSegment {
    reserved_1: u32,
    /// The full 64-bit canonical forms of the stack pointers (RSP) for privilege levels 0-2.
    pub privilege_stack_table: [VirtAddr; 3],
    reserved_2: u64,
    /// The full 64-bit canonical forms of the interrupt stack table (IST) pointers.
    pub interrupt_stack_table: [VirtAddr; 7],
    reserved_3: u64,
    reserved_4: u16,
    /// The 16-bit offset to the I/O permission bit map from the 64-bit TSS base.
    pub iomap_base: u16,
}
```

#### Kotlinのスコープ関数とにている？

中括弧（`{}`）でスコープを作ってその返り値のみを利用する形はKotlinのスコープ関数と書き方が似ている。このように書ける目的は可読性を上げるため？

ただし、Rustの場合、このコードでは`STACK`がグローバルにアクセスできるようになっているのでスコープと言えるかどうか怪しい

```rust
tss.interrupt_stack_table[DOUBLE_FAULT_IST_INDEX as usize] = {
    const STACK_SIZE: usize = 4096 * 5;
    static mut STACK: [u8;STACK_SIZE] = [0;STACK_SIZE];

    let stack_start = VirtAddr::from_ptr(unsafe { &STACK});
    let stack_end = stack_start  + STACK_SIZE;
    stack_end
};
```


#### グローバルディスクリプタテーブル

通称GDT。

>ページングがデファクトスタンダードになる以前は、メモリセグメンテーションのため使われていた古い仕組みです。

ページングと対になる概念らしい。ここに詳しく書かれているが読んでもイマイチ理解できない

>https://blog.ishikawa.tech/entry/2018/12/10/215234

GDTは、プログラムのセグメントを含む構造なのだが、肝心のセグメントの仕組みはもう利用されていない。だが、ユーザーモードとカーネルモードの切り替えやTSSの読み込みのためにまだ利用されている


### Hardware Interrupts

この章から英語のみ。大変。。

#### 割り込み

そもそも割り込みとは、ハードウェアからCPUに対して通知を送る仕組みのことで、CPUが定期的にハードウェアに状態を取得する方法に比べて遥かに効率的。ただし、例外の場合と違い、割り込みは**非同期**的に発生する

割り込みの命令は直にCPUには送信されず、InterruptControllerが仲介する。

####  Intel 8259（=PIC）

現在では`APC`が普及しているため`Intel 8259`が利用されることはないが、インタフェース自体は現在でも後方互換性のためサポートされている場合が多い

典型的なしすてむでは、`Intel 8259`を2台使い、以下のような構成でCPUと接続していた

```
                     ____________                          ____________
Real Time Clock --> |            |   Timer -------------> |            |
ACPI -------------> |            |   Keyboard-----------> |            |      _____
Available --------> | Secondary  |----------------------> | Primary    |     |     |
Available --------> | Interrupt  |   Serial Port 2 -----> | Interrupt  |---> | CPU |
Mouse ------------> | Controller |   Serial Port 1 -----> | Controller |     |_____|
Co-Processor -----> |            |   Parallel Port 2/3 -> |            |
Primary ATA ------> |            |   Floppy disk -------> |            |
Secondary ATA ----> |____________|   Parallel Port 1----> |____________|

```

それぞれのコントローラーはI/Oポートでコマンド,データそれぞれのポートが設定されている。プライマリなコントローラーのコマンドは`0x20`,データは`0x21`。セカンダリなコントローラーのコマンドは`0xa0`,データは`0xa1`。

PICのデフォルトの設定である、0~15の範囲は、既に例外処理のために利用されている（おそらく、この範囲は割り込み記述子表
=IDTのことを指している？）。そのため、PICの割り込み番号を再設定する必要がある

32~47が利用されることが多いらしい

#### Timer割り込み

上の図ではTimerはプライマリなコントローラーの0番目に位置している。なので、Timer割り込み時のハンドラを定義するためには、IDTの32に関数を定義する

Timer割り込みのハンドラを実装後、エミュレータを実行すると、Timer割り込みは頻繁に発生するためハンドラが絶えず実行されることが予想されるが、実際はそうはならず一度のみ実行される。というのも、PICが明示的な割り込み終了（=EOI）が与えられるのをずっと待機しているため。

EOIをPICに送信すると、ハンドラが毎回呼び出されるようになる

```rust
PICS.lock().notify_end_of_interrupt(InterruptIndex::Timer.as_u8());
```

#### IndexMutトレイト

`IndexMut`トレイトが付与されている構造体は、添字でのアクセスが可能。また、取得される要素は可変になる

`Index`トレイトというのも存在するが、これは要素が不変になる

#### デッドロック

割り込み処理が発生した場合、発生前に実行していた処理と割り込み処理で同じリソースをロックするためにデッドロックが発生することが発生する。例えば標準出力に書き込む`WRITER`など。

これは、割り込み発生前処理でリソースをロックする場合は 割り込み処理が発生しないように処理をラッピングすることで防ぐことができる。

```rust
interrupts::without_interrupts(|| {    
  // etc..
});
```

#### The hlt Instruction

main関数やpanic発生時ハンドラの中で、OSを実行したままにするために無限ループ処理を行っているが、これはCPUに対して負荷がかかりすぎる。そのような場合は`hlt`命令を送信し、**次の割り込み処理発生時までCPUを停止させる**ことができる。


#### キーボード割り込み

Timer割り込みと同様の仕組みでキーボード割り込みも実装ができる。

割り込みのベクタは33。ただし、一度割り込みが発生するとキーボードから`Scancodes`を読みとる。そうしないと次のキーボード割り込みが発生しないようになっているため

### Introduction to Paging

メモリ管理のページングについて。

ARMでは、MPU（MemoryProtectionUnit）という仕組みがあり、アクセス権限を定義できる。

x86では、ページングとセグメンテーションによって各プロセスのアクセス権限を定義していた（そもそも今はセグメンテーションは利用されていない）

#### セグメンテーション

1978年ごろ導入された。当時は64KiBしかメモリの要領がなかったが、セグメンテーションを導入することで1MiBまでメモリとして利用可能にした。

セグメントレジスタの説明はここがとてもわかりやすい↓

>https://ja.wikipedia.org/wiki/%E3%82%BB%E3%82%B0%E3%83%A1%E3%83%B3%E3%83%88%E6%96%B9%E5%BC%8F#%E3%83%AA%E3%82%A2%E3%83%AB%E3%83%A2%E3%83%BC%E3%83%89

リアルモードの時代では、セグメンテーションレジスタに直接メモリのアドレスが保存されていたが、プロテクトモードが導入されてから、セグメンテーションレジスタにはGDTまたはLDTのアドレスが配置されるようになった。

GDT/LDTにはメモリのアドレス・セグメントのサイズ（？）・アクセス権限が配置されていて,OSはそれらを読み込むことで適切なアクセス権限をプロセスごとに付与できるようになった

#### フラグメンテーション

連続したメモリ空間が必要な場合があるが、どうしてもメモリ空間の断片化が起こってしまいプログラムを配置することができない。そのような場合にメモリ空間を整理することをフラグメンテーションと呼ぶ。

これは非常に負荷がかかる。そのためx86ではセグメンテーションはサポートされていない

#### ページング

セグメンテーションによるメモリ空間の断片化を小さくできる代替方法。固定のサイズでプログラムを分割してメモリに配置することをさす。分割されたものは`ページ`と呼ばれる。

この方法でもフラグメンテーション自体は発生するが、断片化の量を予測可能にするため優れている

#### ページテーブル

ページごとに物理メモリのどこに配置されるかを保持するもの。x86では`CR3`というレジスタにページテーブルが保存される。

メモリにアクセスする際には、CPUがページテーブルを参照する。これら一連の手順は全てハードウェア側で行われる。スピードアップするためには多くのCPUアーキテクチャでページテーブルをキャッシュする仕組みが利用されている

また、ページテーブル自体のサイズを小さくするため、ページテーブルにも階層が存在する。こうすることでサイズが小さくなる

#### x86でのページ

4階層のページテーブルが存在する。それぞれのページテーブルには、512の要素が格納されており、各要素は、ページへの物理アドレスとアクセス権限が含まれている。

要素あたりのサイズは8Bit、したがってページテーブルの大きさは512*8Bit=4kiB、

仮想アドレスの構造は以下のような感じ。各範囲ごとに、**その階層で何晩目の要素を参照するか**を保存する。各ページテーブルで要素数は512のため、2^9で9bitあればよい計算

|bit|内容|
|--|--|
|0~11|ページのoffset|
|12~20|1階層目のページテーブルのindex|
|21~29|2階層目のページテーブルのindex|
|30~38|3階層目のページテーブルのindex|
|39~47|4階層目のページテーブルのindex|
|48~63|未使用、IntelのIceLakeなどでは5階層目として利用されているらしい|

実際にアクセスする際の流れ

>![](https://os.phil-opp.com/paging-introduction/x86_64-page-table-translation-steps.svg)
>
>https://os.phil-opp.com/paging-introduction/

#### translation lookaside buffer

4階層の場合、ページテーブルを4回参照する必要がある、それを簡略化するために直近のいくつかの変換を`translation lookaside buffer`（=TLB）というところにキャッシュする。

ページテーブルが変更されるとこのTLBに含まれる変換の内容が古今間になってしまうが、そうならないようカーネル側で管理する必要がある

#### CR2レジスタ

`CR2`レジスタには、ページフォールとが発生した際にページフォールとを発生させた**物理アドレス**が保存されている。

### Paging Implementation

#### 

カーネル自体も仮想アドレスの上で動作しているため、ページテーブルの中の物理アドレスを仮想アドレスに変更することは簡単ではない

- Map at a Fixed Offset
  - ページテーブルの物理アドレスおよび仮想アドレスを一定のオフセットでずらして管理する方法。
  - 物理アドレス→仮想アドレスに変換する必要がない
- Map the Complete Physical Memory
  - 全てのデータの物理アドレスおよび仮想アドレスを一定のオフセットでずらして管理する方法。
- Temporary Mapping
  - 一時的に、最下層のレベルの先頭のページテーブルにアクセスに必要になったページテーブル自体へのマッピングを用意する方法
- Recursive Page Tables
  - 任意のページテーブルに、同レベルのページテーブルへのマッピングを持たせるようにする方法
  - CPUはデータにアクセスしていると思っているが実際にはLevel1や2のページテーブルにアクセスしている状況が発生する
  - どのように解決するかは読んでいてもわからない


#### The `entry_point` Macro

`_start`関数は、RustからではなくRust外部のBootloaderから呼び出されるため、引数のチェックがコンパイラによって行われない。

それを行われるようにするために`entry_point`マクロを利用できる。また、Cの呼び出し規約に沿うための記述もマクロが行ってくれる

#### `*mut`

生ポインタ。あくまで型名。`*`で実態を参照する処理が`unsafe`である必要がある

>https://doc.rust-lang.org/book/ch19-01-unsafe-rust.html#dereferencing-a-raw-pointer


#### 実装の流れ

1. `CR3`レジスタから、Level4のページテーブルの**物理アドレス**を取得
1. 物理アドレスとオフセット（カーネル起動時に渡される情報）をもとに論理アドレスを取得し,Rustから生ポインタ経由で取得できるようにする
1. アクセス先の`VirtAddr`から、該当のページテーブルのindexを取得し、そのindexをもとにページテーブルにアクセスし、物理アドレスを取得
1. 上記2,3を階層分繰り返し終わった後の物理アドレスが目的のフレームへの物理アドレス

#### `impl trait`を構造体のプロパティとして保持することはできない

>The problem with this approach is that it's not possible to store an impl Trait type in a struct field currently. It might work someday when named existential types are fully implemented

### Allocator Designs


アロケーターの実装は複雑。例えば[jemalloc](http://jemalloc.net/)というアロケーターは3万行以上のコードで記述されている

3つのカーネルアロケーターを紹介する
#### Bump Allocator

stack allocatorとも呼ばれていて最もシンプル。

allocateされたメモリの容量とallocateされた回数のみを追跡する仕組み、どこからどこまでの部分がallocateされたかどうかは一切保持しない、そのためメモリの開放はまとめて行う。

allocateされるとメモリの未使用部分からつめてallocateされる。

多くの場合ではallocateのカウンタと利用される

#### 内部可変性について

`spin::Mutex`を利用する。

#### `static`について

staticな値を定義する際に`new()`していてエラーになりそうだが、`new()`は`const`な関数であるためコンパイルできる。2018か利用可能になったらしい

```rust
static ALLOCATOR: Locked<BumpAllocator> = Locked::new(BumpAllocator::new());
```


#### Linked List Allocator

ヒープ領域に、次の未使用空間へのポインタを保持させる。利用されている空間を保持する方法ではなく、空いている空間を保持する方法をとっていて面白い。

空いている空間1つ1つをノードと定義すると、

deallocateの場合は、head領域のnextポインタを解放されたノードの先頭アドレスを指しかつ該当ノードのnextポインタをもともとhead領域が指していたノードの先頭アドレスに変更する

allocateの場合は、要求された容量を持つノードを探し、存在すればそのノードをLinkedListから削除。要求する容量とノードの容量を比較し、余裕があれば、その空間を表すノードを新たに定義しLinkedListに追加。その後削除されたノード（=利用されるメモリ範囲）の先頭アドレスをreturn

#### `&'static mut`

危険らしい。

>ところでこの static mutは "UB-happy" (trigger-happy由来の造語?) であるとして削除が提案される程度には物騒な代物です。そこで、どれくらい危険か、また競プロという文脈で代替策はないか書いてみたいと思います。
>
>https://qiita.com/qnighy/items/46dbf8d2aff7c2531f4e


#### `mem::align_of::<ListNode>()`

対象の構造体に最低限必要なアラインメント？アラインメントがなんなのかいまいち掴めていない...

```rust
assert_eq!(4, mem::align_of::<i32>()); // OK
```

#### Optionからの値の取得について

以下の2つの記述は一緒（のはず）

`ref mut` と `as_mut` は使うタイミングが実は似ているかもしれない... 🤔

```rust
while let Some(ref mut region) = current.next {
    if let Ok(alloc_start) = Self::alloc_from_region(&region, size, align) {
        // region suitable for allocation -> remove node from list
        let next = region.next.take();
        let ret = Some((current.next.take().unwrap(), alloc_start));
        current.next = next;
        return ret;
    } else {
        // region not suitable -> continue with next region
        current = current.next.as_mut().unwrap();
    }
}
None
```

```rust
loop {
    if current.next.is_none() {
        return None;
    }

    let o = current.next.as_mut().unwrap();
    if let Ok(alloc_start) = Self::alloc_from_region(o, size, align) {

        let next = o.next.take();
        let ret = Some((current.next.take().unwrap(), alloc_start));
        current.next = next;
        return ret;
    } else {
        // region not suitable -> continue with next region
        current = current.next.as_mut().unwrap();
    }
}
```

#### Fixed-Size Block Allocator

固定長のブロックで管理する方法。固定長のブロックの大きさはいくつか種類があり、指数関数的に用意されているらしい。

allocateされた場合、要求された容量を満たす大きさのブロックの種類を探し該当ブロック全体を利用済みとして記録する。ブロックを探す際の計算量が`O(1)`であるため、LinkedListよりも高速に動く。

##### Fallback Allocator

用意されたブロック長を超えたサイズでallocateされた場合、対応できない。そのためこのような場合のみLinkedListのアロケータを利用することができる。

LinkedListのアロケータはノードの数が増えるとtraversalに時間がかかるが、この場合、全てLinkedListなアロケータに比べて数が少なくなることが予想されるのでそこまで問題にはならない

### Async/Await

#### Preemptive Multitasking

CPUのプリエンティブ割り込みの話。

ハードウェア割り込みが入ると、処理が`IDT`に定義されている割り込みハンドラに移動する。これによってOSが制御することができる。また、割り込み時、割り込みされるタスクの状態が全て保存される。この流れをコンテキストスイッチという

OSは、タイマ割り込みができるため、OSが完全にコントロールできる

通常はコールスタックは大きいため、それぞれのタスクごとに分けられている。そのおかげでコンテキストスイッチのタイミングではレジスタのみを移動させるだけで済む。

#### Cooperative Multitasking

各タスクが完了するまでOSが待つ、タスク自身が停止させることもできる。この仕組みは言語レベルで利用されることが多い。

開発者やコンパイラが`yield`命令（CPUのコントロールを手放す命令）を挿入することで実現できる。非同期の処理と組み合わせられることが多い、

OSが状態を保持させる必要はないが、代わりにタスクがコントロールを手放すときに復帰する際に状態を保存する必要がある。逆にいうとそれ以外のコールスタックのデータは全て破棄される。すなはち1ツノコールスタックを全タスクが共有する


#### Futures

Rustでは非同期タスクを`Future`トレイトでサポートしている

```rust
pub trait Future {
    type Output;
    fn poll(self: Pin<&mut Self>, cx: &mut Context) -> Poll<Self::Output>;
}

pub enum Poll<T> {
    Ready(T),
    Pending,
}
```

`poll()`の引数`cx: &mut Context`は`Waker`インスタンスを渡すため。


Futureのコンビネータはstdライブラリには含まれていないが、`future`クレートやその他クレートで提供されている。

Futureのコンビネータは、型システムを複雑にしてしまう場合がある。以下のような実装では以下に注意する必要がある

- ライフタイムの制限のため、`move`が必要
- 右辺左辺で型を一致させるため`Either`構造体でラップする必要がある
- future構造体を返却しない`else句`では、`Poll::Ready(T)`を返すため`future::ready(content)`を利用する必要がある

```rust
fn example(min_len: usize) -> impl Future<Output = String> {
    async_read_file("foo.txt").then(move |content| {
        if content.len() < min_len {
            Either::Left(async_read_file("bar.txt").map(|s| content + &s))
        } else {
            Either::Right(future::ready(content))
        }
    })
}
```

これらを簡単にするために`async/await`が利用できる。（ここらへんはjsと全て一緒）

```rust
async fn example(min_len: usize) -> String {
    let content = async_read_file("foo.txt").await;
    if content.len() < min_len {
        content + &async_read_file("bar.txt").await
    } else {
        content
    }
}
```

`async`修飾子が指定されていると、コンパイラはステートマシンを組み立てる

コンテキストスイッチ発生時、コンパイラがその状態を保存するためにいくつかの構造体を作りそこにデータを格納する。

ステートマシンの各状態ごとに構造体ができるイメージ

>![](https://os.phil-opp.com/async-await/async-state-machine-basic.svg)

#### Pinning

自己参照構造体、

```rust
async fn pin_example() -> i32 {
    let array = [1, 2, 3];
    let element = &array[2];
    async_write_file("foo.txt", element.to_string()).await;
    *element
}
```

このような関数は、ステート構造体として表現すると、`array`と`element`を保持する構造体になる。elementはarrayの最後の要素のアドレスを指したいが、ステート構造体の配置が変わりそれによってarrayの配置も変わる場合がある

これを解消するためにRustは`Pin`トレートを導入して、構造体の移動を禁止している。この方法が選ばれた背景には、この方法がゼロコスト抽象化であることがある

`Pin`についてもっと知るために読んでみたがいまいち掴めない↓

>https://tech-blog.optim.co.jp/entry/2020/03/05/160000?utm_source=feed
